﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySystem.Models;
using MySystem.Repository;

namespace MySystem.Service
{
    public class RepositoryService
    {
        private IRepository _repository;
        public RepositoryService(IRepository repository)
        {
            _repository = repository;
        }
        public List<EmployeeDetails> AllEmployee()
        {
            return _repository.AllEmployee();
        }

        public EmployeeDetails getDetailsEmployee(int ID)
        {
            return _repository.getDetailsEmployee(ID);
        }
        public List<Users> getUsers()
        {
            return _repository.getUsers();
        }

        public Users getUsersByID(int id)
        {
            return _repository.getUsersByID(id);
        }
    }
}
