﻿using MySystem.Models;
using MySystem.Repository;
using MySystem.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MySystem
{
    public partial class MyAccount : Form
    {

        public RepositoryService _repo = new RepositoryService(new StaticRepository());
        public MyAccount()
        {
            InitializeComponent();

        }

        private void MyAccount_Load(object sender, EventArgs e)
        {
            //get current user info
            Users current_user = _repo.getUsersByID(Program.current_user_id);
            lbl_username.Text = current_user.username;

            //load Employee
            dataGridView.ColumnCount = 4;
            dataGridView.Columns[0].Name = "ردیف";
            dataGridView.Columns[0].Width = 40;
            dataGridView.Columns[1].Name = "نام";
            dataGridView.Columns[2].Name = "حقوق";
            dataGridView.Columns[3].Name = "جنسیت";
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.MultiSelect = false;


            List<EmployeeDetails> EmployeeDetailsList = _repo.AllEmployee();
            int i = 0;
            foreach(var item in EmployeeDetailsList)
            {
                i++;
                dataGridView.Rows.Add(i,item.Name,item.salary,item.Geneder);
            }

        }
    }
}
