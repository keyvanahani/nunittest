﻿using MySystem.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MySystem
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt_username.Text) || string.IsNullOrEmpty(txt_password.Text))
            {
                MessageBox.Show("Userid or password could not be Empty.");
            }
            else
            {
                if(handler.Login(txt_username.Text, txt_password.Text) == true)
                {
                    this.Hide();
                    MyAccount myaccount_form = new MyAccount();
                    myaccount_form.Closed += (s, args) => this.Close();
                    myaccount_form.Show();
                }
                else
                {
                    MessageBox.Show("Incorrect UserId or Password.");
                }
            }
            //Login(txt_username.Text, txt_password.Text);
        }

       
    }
}
