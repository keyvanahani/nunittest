﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySystem.Models;

namespace MySystem.Repository
{
    public interface IRepository
    {
        List<EmployeeDetails> AllEmployee();
        EmployeeDetails getDetailsEmployee(int id);
        List<Users> getUsers();
        Users getUsersByID(int id);
    }
}
