﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySystem.Models;

namespace MySystem.Repository
{
    public class StaticRepository : IRepository
    {
        public List<EmployeeDetails> AllEmployee()
        {
            List<EmployeeDetails> li = new List<EmployeeDetails>();
            li.Add(new EmployeeDetails
            {
                id = 100,
                Name = "Bharat",
                Geneder = "male",
                salary = 40000
            });
            li.Add(new EmployeeDetails
            {
                id = 101,
                Name = "sunita",
                Geneder = "Female",
                salary = 50000
            });
            li.Add(new EmployeeDetails
            {
                id = 103,
                Name = "Karan",
                Geneder = "male",
                salary = 40000
            });
            li.Add(new EmployeeDetails
            {
                id = 104,
                Name = "Jeetu",
                Geneder = "male",
                salary = 23000
            });
            li.Add(new EmployeeDetails
            {
                id = 105,
                Name = "Manasi",
                Geneder = "Female",
                salary = 80000
            });
            li.Add(new EmployeeDetails
            {
                id = 106,
                Name = "Ranjit",
                Geneder = "male",
                salary = 670000
            });
            return li;
        }

        public EmployeeDetails getDetailsEmployee(int id)
        {
            EmployeeDetails EmployeeDetails = new EmployeeDetails();
            List<EmployeeDetails> li = AllEmployee();
            foreach (var x in li)
            {
                if (x.id == id)
                {
                    EmployeeDetails = x;
                }
            }
            return EmployeeDetails;
        }

        public List<Users> getUsers()
        {
            List<Users> li = new List<Users>();
            li.Add(new Users
            {
                id = 1,
                username = "admin",
                password = "123456"
            });
            li.Add(new Users
            {
                id = 2,
                username = "user1",
                password = "123456"
            });

            li.Add(new Users
            {
                id = 3,
                username = "user2",
                password = "123456"
            });

            li.Add(new Users
            {
                id = 4,
                username = "user3",
                password = "123456"
            });
            return li;
        }

        public Users getUsersByID(int id)
        {
            Users user = new Users();
            List<Users> li = getUsers();
            foreach (var x in li)
            {
                if (x.id == id)
                {
                    user = x;
                }
            }
            return user;
        }
    }
}
