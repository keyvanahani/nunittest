﻿using MySystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MySystem.Models;

namespace MySystem
{
   public static class Program
    {

        public static int current_user_id;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            current_user_id = 0;
            Application.Run(new login());
        }

       
    }
}
