﻿using MySystem.Service;
using MySystem;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace NUnitTestProject1
{
   public class FunctionTest
    {
        /*https://docs.nunit.org/articles/nunit/writing-tests/assertions/assertion-models/classic.html*/
        [Test]
        public void TestLogin()
        {
            //type 1
            bool A = handler.Login("admin", "987654321");
            Assert.AreEqual(false, A);

            //type 2
            Assert.False(handler.Login("admin", "123456789"));
            Assert.True(handler.Login("admin", "123456"));
      
        }

        [Test, Timeout(300)]
        public void CheckCurrentUserId()
        {
            //Before login
            Assert.That(Program.current_user_id, Is.Zero);


            //Run Login
            handler.Login("admin", "123456");


            //After login
            Assert.That(Program.current_user_id, Is.Not.Zero);
        }



    }

 
}
