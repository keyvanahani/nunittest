using MySystem.Models;
using MySystem.Repository;
using MySystem.Service;
using NUnit.Framework;
using System.Collections.Generic;

namespace NUnitTestProject1
{
    
    public class ModelsTests
    {
        public RepositoryService _RepositoryService;
        public List<Users> li_users;

        [SetUp]
        public void Setup()
        {
            _RepositoryService = new RepositoryService(new StaticRepository());
        }

        [Test]
        public void UsersModelNotNull()
        {
            li_users = _RepositoryService.getUsers();
            foreach (var x in li_users)
            {
                 Assert.IsNotNull(x.id);
                 Assert.IsNotNull(x.username);
                 Assert.IsNotNull(x.password);
            }
        }

        [Test]
        public void EmployeeModelNotNull()
        {
            var li_Employee = _RepositoryService.AllEmployee();
            foreach (var x in li_Employee)
            {
                Assert.IsNotNull(x.id);
                Assert.IsNotNull(x.Name);
                Assert.IsNotNull(x.salary);
                Assert.IsNotNull(x.Geneder);

            }
        }



    }
}