﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySystem.Repository;
using MySystem.Service;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestLogin()
        {
            bool y = handler.Login("demo", "123456");
            bool x = handler.Login("demo2", "654321");
            bool c = handler.Login("admin", "admin");
            Assert.AreEqual(false, y);
            Assert.AreEqual(false, x);
            Assert.AreEqual(true, c);

        }


        [TestMethod]
        public void Checkdetails()
        {
            RepositoryService _RepositoryService = new RepositoryService(new StaticRepository());
            var li = _RepositoryService.AllEmployee();

            foreach (var x in li)
            {
                Assert.IsNotNull(x.id);
                Assert.IsNotNull(x.Name);
                Assert.IsNotNull(x.salary);
                Assert.IsNotNull(x.Geneder);
               
            }
        }



    }
}
